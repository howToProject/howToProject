# howToProject

## Some Projects

### Consumables

A simple app to help manage printer (or other) consumables. You can find needed consumables in three steps and get back a url where you can place your order. The configuration needs to be maintained manually in three .json files. You can archive ordered consumables in a Sqlite databse if you want to keep a history.

https://gitlab.com/howto-consumables

### DemocracyHelper

This webapp allows people participating in a (community) project to make proposals and comment on them, using per project defined comment categories.

https://gitlab.com/howto-democracy_helper

### PushAndTrigger

A public template and instructions that allow us to push to a repo and deploy to a custom server using only one command. The principle of course also works for any other gitlab actions.

https://gitlab.com/howto-automated_deployment

### GuidFactory

Command line tool that gives unique ids.
https://gitlab.com/howToProject/guid_factory


## Examples and cheatsheets

* [/cheatsheets](https://gitlab.com/howToProject/cheatsheets)
* [/.../custom_actions.example](https://gitlab.com/howto-automated_deployment/custom_actions.example)
* [/bind9_example](https://gitlab.com/howToProject/bind9_example)
* [/avalonia_example](https://gitlab.com/howToProject/avalonia_example)
* [/angular_example](https://gitlab.com/howToProject/angular_example)